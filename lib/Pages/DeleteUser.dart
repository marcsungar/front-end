import 'package:flutter/material.dart';
import 'package:killing_time_notepatch/Widgets/BasicOnPage.dart';

class DeleteUser extends StatefulWidget {
  static const routeName = '/DeleteUser';

  @override
  State<StatefulWidget> createState() => _DeleteUser();
}

class _DeleteUser extends State<DeleteUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BasicOnPage.ApBar("Delete User", Colors.red),
      body: Text("hola"),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: BasicOnPage.currentIndex,
        onTap: (value) {
          setState(
                () => BasicOnPage.currentIndex = value,
          );
          BasicOnPage.onTabTapped(context, BasicOnPage.currentIndex);
        },
        selectedItemColor: Colors.black,
        items: BasicOnPage.MenuBot(context),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:killing_time_notepatch/Widgets/BasicOnPage.dart';

class GetUser extends StatefulWidget {
  static const routeName = '/GetUser';

  @override
  State<StatefulWidget> createState() => _GetUser();
}

class _GetUser extends State<GetUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BasicOnPage.ApBar("Get User", Colors.teal),
      body: Text("hola"),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: BasicOnPage.currentIndex,
        onTap: (value) {
          setState(
                () => BasicOnPage.currentIndex = value,
          );
          BasicOnPage.onTabTapped(context, BasicOnPage.currentIndex);
        },
        selectedItemColor: Colors.black,
        items: BasicOnPage.MenuBot(context),
      ),
    );
  }
}
